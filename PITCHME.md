
# Hello, Welcome

---

# FAQs
##### A Q/A Based Introduction to New Capability
---

#I, You and Why?

+++

#I
<br>
##### Representative of DevOps
+++

#You
<br>
* Leaders
* Testing Partners
* Other Stakeholders

+++

#Why?

+++

### We have created 
### A New DevOps Capability

+++

## We Believe
<br>
* Its Disruptive
* Has potential to create Org wide impact

+++

## Pitch It, Demo It and Create Noise
<br>
* Grab your attention
* Ask for investment

---

## What is DevOps?

+++
<br>
<br>
## DevOps = Development + Operations
<br>
<br>
<br>
<br>
##### ~~Solo Initative To Jack Up My Contract~~

---

## Any Useful Output?

+++
<br>
## Oh, Yes!
<img src="assets/images/devops.png" alt="DevOps" style="height: 400px; width: 900px;"/>

---

## Why was 'sbb-test' created?

+++

## To solve these problems
<br>
* Environment Testing
* CCL (Report/Extract) Testing

---

## What is Environment Testing?

+++
<br>
#### Testing the presence/absence in any domain
|       Powerforms      |     Nomenclatures    |
|:---------------------:|:--------------------:|
|        Sections       | Nomenclature Aliases |
|          DTAs         |      CCL Objects     |
|      Code Values      |     Include Files    |
|   Code Value Aliases  |  Explorer Menu Entry |
| Code Value Extensions |        Orders        |
|       Code Sets       |    Any Other Build   |

+++

## Input
<br>
* CSV Files
* DCWs

+++

## Can be run 
<br>
### Anytime
#####On Demand
<br>
### Every time
#####Before Deployment, Running the report etc.

+++


<img src="assets/images/test_na.gif" alt="Test NA" style="height: 450px; width: 900px;"/>

+++

## Score Card

+++

### <font color="#000000"><u>1008</u></font> Nomenclatures Tested 
<br>
### in 
<br>
### <font color="#000000"><u>50</u></font> Seconds

---

## What is CCL Testing?

+++

#### Automated Testing
##### CCL - Plain / Report / Extract
<br>
#### Powerform Based
##### Automatic Charting / Removal

---

## How does it work?

---
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_1.png" alt="Step 1" style="height: 300px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_2.png" alt="Step 2" style="height: 300px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_3.png" alt="Step 3" style="height: 300px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_4.png" alt="Step 4" style="height: 300px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_5.png" alt="Step 5" style="height: 500px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_6.png" alt="Step 6" style="height: 500px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_7.png" alt="Step 7" style="height: 600px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_8.png" alt="Step 8" style="height: 600px; width: 900px;"/>

+++
<!-- .slide: data-background-transition="none" -->
<img src="assets/images/wf_9.png" alt="Step 9" style="height: 600px; width: 900px;"/>

---

## Is it fictional?

---

## No. Its not even a POC.
<br>
## Being used for
<br>
* Burns
* Height and Weight

---

## Burns Stats

+++

* #### Powerforms           - <font color="#000000">4</font>            <!-- .element: class="fragment" -->
* #### Code Values          - <font color="#000000">254</font>          <!-- .element: class="fragment" -->
* #### Nomenclatures        - <font color="#000000">1008</font>         <!-- .element: class="fragment" -->
* #### Nomenclature Aliases - <font color="#000000">1008</font>         <!-- .element: class="fragment" -->
* #### CCL Script           - <font color="#000000">4500 LOC</font>     <!-- .element: class="fragment" -->
* #### Defects              - <font color="#000000">??</font>           <!-- .element: class="fragment" -->

---

<img src="assets/images/pf_test.gif" alt="Test NA" style="height: 450px; width: 900px;"/>

+++

## Score Card

+++

### <font color="#000000"><u>1000</u></font> Iterations Tested 
<br>
### in 
<br>
### <font color="#000000"><u>50</u></font> Minutes

---

## Utilities
##### To make more lazy

+++

### Seed File Generator
<img src="assets/images/gen_seed_file.gif" alt="Test NA" style="height: 450px; width: 900px;"/>

+++

### Powerform Remover
<img src="assets/images/remove_pf.gif" alt="Test NA" style="height: 450px; width: 900px;"/>

---

## How does it compare to Vedant?

+++

![apple-orange](assets/images/apple-orange.jpg)
+++
###CCL Testing   
<font color="#000000">vs</font>   
###UI Testing  
+++       
###Drop A Folder   
<font color="#000000">vs</font>   
###Per Machine - Thick Client
+++ 
###Command Line   
<font color="#000000">vs</font>   
###UI Based
+++          
###Automatic    
<font color="#000000">vs</font>   
###Manual
+++          
###Developers   
<font color="#000000">vs</font>   
###Testers

---

## How is this disruptive?

+++

### Solves a problem no other tool can do

+++

### No change to any workflow

+++

### 100% Coverage

+++

### Smart Case Generator

+++

### Portable - Tool and Data

+++

###2 sec  
<font color="#000000">vs</font>   
###20 mins

---

## How does it affect eHealth?

+++

### No more testers needed

+++

### Accelerates LHD Testing

+++

### Changes the way IS delivers fixes

---

### Acknowledgements

#### Prasad
######Powerform Saver

#### Patrick
######Postman Introducer

#### Leslie
######Programmer Exceptionnel
<br>
<p style="font-size:6px"><i>And last but not the least.....Takahashi</i></p>

---

# Thanks for your time